document.addEventListener('DOMContentLoaded', function () {
    const artworksContainer = document.getElementById('artworksContainer');
    const playStopButton = document.getElementById('playStopButton');
    const audioElement = new Audio();
    let playlist = [];
    let currentTrackIndex = 0;
    let isPlaying = true; // Assume playback starts in playing state
    const assetsPath = "../assets/"; // Adjust based on your setup

    function hideLoadingScreen() {
        document.getElementById('loadingScreen').style.display = 'none';
        artworksContainer.style.display = 'flex';
    }
    function fetchPlaylist() {
        fetch(`${assetsPath}playlist.json`)
            .then(response => response.json())
            .then(data => {
                playlist = data;
                initializePlayer(playlist);
                hideLoadingScreen();
            })
            .catch(error => {
                console.error('Failed to load playlist:', error);
                hideLoadingScreen();
            });
    }

    function initializePlayer(playlist) {
        playlist.forEach((track, index) => {
            const img = document.createElement('img');
            img.src = `${assetsPath}${track.artwork}`;
            img.alt = track.title;
            img.classList.add('artwork-image');
            img.addEventListener('click', () => {
                playTrack(track, index);
                isPlaying = true; // Audio will start playing
                updatePlayStopButtonState(); // Update button to reflect playing state
            });

            const artworkDiv = document.createElement('div');
            artworkDiv.className = 'artwork';
            artworkDiv.appendChild(img);
            artworksContainer.appendChild(artworkDiv);
        });
        setupMediaSession();
    }

 function playTrack(track, index, force = false) {
        // Calculate the sync offset only if we are forcing play or changing the track
        if (force || audioElement.src !== `${assetsPath}${track.src}`) {
            const offset = calculateSyncOffset(track.duration);
            audioElement.src = `${assetsPath}${track.src}`;
            audioElement.addEventListener('loadedmetadata', () => {
                audioElement.currentTime = offset / 1000; // Convert ms to seconds for currentTime
                audioElement.loop = true;
                audioElement.play().then(() => {
                    isPlaying = true;
                    updatePlayStopButtonState();
                    updateMediaSession(track);
                    updateArtworkSaturation(index);
                }).catch(err => console.error('Playback initiation failed:', err));
            }, { once: true });
        } else if (!isPlaying) { // Check if we're resuming the current track without forcing reload
            audioElement.play().then(() => {
                isPlaying = true;
                updatePlayStopButtonState();
            }).catch(err => console.error('Playback initiation failed:', err));
        }
        currentTrackIndex = index; // Update the current track index
    }

    function updatePlayStopButtonState() {
        playStopButton.style.display = 'flex'; // Ensure the button is shown if it was initially hidden
        playStopButton.textContent = isPlaying ? 'Stop' : 'Play';
        playStopButton.style.visibility = "visible";
        playStopButton.style.opacity = 1;
    }

    function updateArtworkSaturation(currentIndex) {
        document.querySelectorAll('.artwork-image').forEach((img, idx) => {
            img.style.filter = idx === currentIndex ? 'grayscale(0) brightness(100%)' : 'grayscale(100%) brightness(60%)';
        });
    }
    
    playStopButton.addEventListener('click', function() {
        if (isPlaying) {
            audioElement.pause();
            isPlaying = false;
        } else {
            // When resuming, ensure playback is in sync
            const track = playlist[currentTrackIndex];
            playTrack(track, currentTrackIndex, true);
        }
        updatePlayStopButtonState();
    });

    

    audioElement.addEventListener('ended', () => {
        // Since loop is true, this is just a fallback
        playTrack(playlist[currentTrackIndex], currentTrackIndex);
    });


    function fetchPlaylist() {
        fetch(`${assetsPath}playlist.json`)
            .then(response => response.json())
            .then(data => {
                playlist = data;
                initializePlayer(playlist);
                hideLoadingScreen();
            })
            .catch(error => {
                console.error('Failed to load playlist:', error);
                hideLoadingScreen();
            });
    }

    function initializePlayer(playlist) {
        playlist.forEach((track, index) => {
            const img = document.createElement('img');
            img.src = `${assetsPath}${track.artwork}`;
            img.alt = track.title;
            img.classList.add('artwork-image'); // Add a class for targeting
            img.addEventListener('click', () => {
                playTrack(track, index);
            });

            const artworkDiv = document.createElement('div');
            artworkDiv.className = 'artwork';
            artworkDiv.appendChild(img);
            artworksContainer.appendChild(artworkDiv);
        });
        setupMediaSession();
    }

    function calculateSyncOffset(trackDuration) {
        const now = new Date();
        const midnight = new Date(Date.UTC(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(), 0, 0, 0));
        const msSinceMidnightUTC = now - midnight;
        return msSinceMidnightUTC % trackDuration;
    }



    function updateMediaSession(track) {
        if ('mediaSession' in navigator) {
            navigator.mediaSession.metadata = new MediaMetadata({
                title: track.title,
                artist: track.artist,
                album: track.album,
                artwork: [{ src: `${assetsPath}${track.artwork}`, sizes: '512x512', type: 'image/png' }],
            });

            navigator.mediaSession.setActionHandler('previoustrack', previousTrack);
            navigator.mediaSession.setActionHandler('nexttrack', nextTrack);
        }
    }

    function previousTrack() {
        if (currentTrackIndex === 0) {
            currentTrackIndex = playlist.length - 1; // Loop to the end
        } else {
            currentTrackIndex -= 1;
        }
        playTrack(playlist[currentTrackIndex], currentTrackIndex);
    }

    function nextTrack() {
        if (currentTrackIndex === playlist.length - 1) {
            currentTrackIndex = 0; // Loop back to the start
        } else {
            currentTrackIndex += 1;
        }
        playTrack(playlist[currentTrackIndex], currentTrackIndex);
    }

    fetchPlaylist();
});