const CACHE_NAME = 'dynamic-playlist-cache-v1';
const STATIC_ASSETS = [
    '/',
    '/index.html',
    '/style.css',
    //'/script.js',
    // Add other static assets as needed
];

// Install event
self.addEventListener('install', event => {
    event.waitUntil(
        Promise.all([
            caches.open(CACHE_NAME)
                .then(cache => {
                    console.log('Service Worker: Caching Static Assets');
                    return cache.addAll(STATIC_ASSETS);
                }),
            cachePlaylistContent() // Ensure this function's promises are awaited
        ]).then(() => self.skipWaiting())
    );
});


// Activate event
self.addEventListener('activate', event => {
    event.waitUntil(
        caches.keys().then(cacheNames => {
            return Promise.all(cacheNames.filter(cacheName => cacheName !== CACHE_NAME)
                .map(cacheName => caches.delete(cacheName))
            );
        }).then(() => self.clients.claim()) // Claim clients immediately
    );
});

// Fetch event to cache dynamic playlist content
self.addEventListener('fetch', event => {
    event.respondWith(
        caches.match(event.request)
            .then(response => {
                // If a cache hit is found, return the cached response
                if (response) {
                    return response;
                }
                return fetch(event.request).then(
                    response => {
                        // Check if we received a valid response
                        if (!response || response.status !== 200 || response.type !== 'basic') {
                            return response;
                        }

                        const responseToCache = response.clone();

                        caches.open(CACHE_NAME)
                            .then(cache => {
                                cache.put(event.request, responseToCache);
                            });

                        return response;
                    }
                );
            })
    );
});




// Function to cache dynamic playlist content
function cachePlaylistContent() {
    fetch('../assets/playlist.json') // Adjust path as necessary
        .then(response => response.json())
        .then(playlist => {
            const mediaUrls = playlist.map(track => [
                `../assets/${track.src}`, // Adjust path as necessary
                `../assets/${track.artwork}` // Adjust path as necessary
            ]).flat();
            caches.open(CACHE_NAME).then(cache => cache.addAll(mediaUrls));
        })
        .catch(error => console.error('Failed to cache playlist content:', error));
}

// Call the function to cache playlist content after service worker installation
self.addEventListener('install', () => {
    cachePlaylistContent();
});
