document.addEventListener('DOMContentLoaded', function () {
    const audioPlayer = document.getElementById('audioPlayer');
    const prevButton = document.getElementById('prevButton');
    const nextButton = document.getElementById('nextButton');
    const playButton = document.getElementById('playButton');
    let currentArtwork = document.getElementById('currentArtwork');
    let feedback = document.getElementById('feedback');

    let playlist = [
        {
            src: '../../assets/audio-001.mp3',
            title: 'First Track',
            artist: 'Artist Name',
            album: 'Album Title',
            artwork: '../../assets/audio1.drawio.png',
        },
        {
            src: '../../assets/audio-002.mp3',
            title: 'Second Track',
            artist: 'Artist Name',
            album: 'Album Title',
            artwork: '../../assets/audio2.drawio.png',
        },
        {
            src: '../../assets/audio-003.mp3',
            title: 'Third Track',
            artist: 'Artist Name',
            album: 'Album Title',
            artwork: '../../assets/audio3.drawio.png',
        },
    ];

    let currentTrack = 0;
    let isLooping = true; // Ensure tracks loop by default

    function updateFeedback(message) {
        if (feedback) {
            feedback.textContent = message;
        } else {
            console.warn("Feedback element not found.");
        }
    }

    function loadTrack(trackIndex, shouldLoop) {
        const track = playlist[trackIndex];
        audioPlayer.src = track.src;
        audioPlayer.loop = shouldLoop; // Set the looping behavior based on shouldLoop flag
        
        if (currentArtwork) {
            currentArtwork.src = track.artwork;
            currentArtwork.alt = `Artwork for ${track.title} by ${track.artist}`;
        } else {
            console.warn("currentArtwork element not found.");
        }

        updateFeedback(`Now Playing: ${track.title} by ${track.artist}`);

        audioPlayer.play().then(() => {
            updateFeedback(`Playing: ${track.title} by ${track.artist}`);
        }).catch(error => {
            console.error("Playback failed", error);
            updateFeedback('Playback failed. Please click Play to start.');
        });

        if ('mediaSession' in navigator) {
            navigator.mediaSession.metadata = new MediaMetadata({
                title: track.title,
                artist: track.artist,
                album: track.album,
                artwork: [{ src: track.artwork, sizes: '96x96', type: 'image/png' }],
            });

            navigator.mediaSession.setActionHandler('previoustrack', () => {
                playPrevious(isLooping);
            });

            navigator.mediaSession.setActionHandler('nexttrack', () => {
                playNext(isLooping);
            });
        }
    }

    function playCurrentTrack() {
        loadTrack(currentTrack, isLooping);
    }

    function playNext(shouldLoop) {
        currentTrack = (currentTrack + 1) % playlist.length;
        loadTrack(currentTrack, shouldLoop);
    }

    function playPrevious(shouldLoop) {
        currentTrack = (currentTrack - 1 + playlist.length) % playlist.length;
        loadTrack(currentTrack, shouldLoop);
    }

    playButton.addEventListener('click', function() {
        playCurrentTrack(); // Loop flag is considered inside this function
        this.style.display = 'none';
    });

    prevButton.addEventListener('click', () => playPrevious(isLooping));
    nextButton.addEventListener('click', () => playNext(isLooping));

    audioPlayer.addEventListener('ended', () => {
        if(isLooping) {
            audioPlayer.currentTime = 0;
            audioPlayer.play();
        }
    });
});