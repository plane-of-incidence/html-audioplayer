document.addEventListener('DOMContentLoaded', function () {
    const audioPlayer = document.getElementById('audioPlayer');
    const prevButton = document.getElementById('prevButton');
    const nextButton = document.getElementById('nextButton');
    const feedback = document.getElementById('feedback');
    const tracksContainer = document.getElementById('tracksContainer');

    let playlist = [
        {
            src: '../../assets/audio-001.mp3',
            title: 'First Track',
            artist: 'Artist Name',
            album: 'Album Title',
            artwork: '../../assets/audio1.drawio.png',
            length: 60480,
        },
        {
            src: '../../assets/audio-002.mp3',
            title: 'Second Track',
            artist: 'Artist Name',
            album: 'Album Title',
            artwork: '../../assets/audio2.drawio.png',
            length: 60480,
        },
        {
            src: '../../assets/audio-003.mp3',
            title: 'Third Track',
            artist: 'Artist Name',
            album: 'Album Title',
            artwork: '../../assets/audio3.drawio.png',
            length: 60480,
        },
    ];
    
    let currentTrack = 0;
    let startTimeUTC = new Date().getTime(); // Example start time; replace with actual UTC start time

    function updateFeedback(message) {
        if (feedback) {
            feedback.textContent = message;
        } else {
            console.warn("Feedback element not found.");
        }
    }

    function populateTracks() {
        playlist.forEach((track, index) => {
            const trackDiv = document.createElement('div');
            trackDiv.classList.add('trackItem');
            trackDiv.innerHTML = `<img src="${track.artwork}" alt="Artwork for ${track.title}">`;
            trackDiv.addEventListener('click', () => {
                currentTrack = index;
                playCurrentTrack();
            });
            tracksContainer.appendChild(trackDiv);
        });
    }

    function calculateOffset() {
        const nowUTC = new Date().getTime();
        const offset = (nowUTC - startTimeUTC) % playlist[currentTrack].length;
        return offset;
    }

    function loadTrack(trackIndex) {
        const track = playlist[trackIndex];
        audioPlayer.src = track.src;

        document.querySelectorAll('.trackItem').forEach((item, index) => {
            if (index === trackIndex) {
                item.classList.add('playing');
            } else {
                item.classList.remove('playing');
            }
        });

        updateFeedback(`Now Playing: ${track.title} by ${track.artist}`);

        audioPlayer.load(); // Ensure we're loading the latest source
        audioPlayer.addEventListener('canplaythrough', () => {
            audioPlayer.currentTime = calculateOffset() / 1000; // Convert offset to seconds
            audioPlayer.play();
        }, { once: true });
    }

    function playCurrentTrack() {
        loadTrack(currentTrack);
    }

    function playNext() {
        currentTrack = (currentTrack + 1) % playlist.length;
        playCurrentTrack();
    }

    function playPrevious() {
        currentTrack = (currentTrack - 1 + playlist.length) % playlist.length;
        playCurrentTrack();
    }

    prevButton.addEventListener('click', playPrevious);
    nextButton.addEventListener('click', playNext);

    audioPlayer.addEventListener('ended', playNext); // Automatically go to the next track when one ends

    populateTracks(); // Call to populate tracks on page load
});