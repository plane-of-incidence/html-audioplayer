document.addEventListener('DOMContentLoaded', function () {
    const tracksContainer = document.getElementById('tracksContainer');
    const feedback = document.getElementById('feedback');
    let audioElement = new Audio();
    let currentTrack = 0;


    let playlist = [
        {
            src: '../../assets/audio-001.mp3',
            title: 'First Track',
            artist: 'Artist Name',
            album: 'Album Title',
            artwork: '../../assets/audio1.drawio.png',
            length: 60480,
        },
        {
            src: '../../assets/audio-002.mp3',
            title: 'Second Track',
            artist: 'Artist Name',
            album: 'Album Title',
            artwork: '../../assets/audio2.drawio.png',
            length: 60480,
        },
        {
            src: '../../assets/audio-003.mp3',
            title: 'Third Track',
            artist: 'Artist Name',
            album: 'Album Title',
            artwork: '../../assets/audio3.drawio.png',
            length: 60480,
        },
    ];

     // Initialize audio elements and preload the first track
    function initializeAudio() {
        audioElement.preload = 'auto';
        audioElement.loop = true; // Ensure track loops
        setTrack(currentTrack); // Start with the first track
    }

    // Set the current track for playback
    function setTrack(trackIndex) {
        const track = playlist[trackIndex];
        audioElement.src = track.src; // Change the source
        audioElement.load(); // Preload the new source
        updateFeedback(`Ready to play: ${track.title}`);
        updateArtworkSaturation(trackIndex);
        updateMediaSession(trackIndex); // Update Media Session for lock screen
    }

    // Play the current track, ensuring no other track is playing
    function playTrack() {
        if (!audioElement.paused) {
            audioElement.pause();
        }
        audioElement.play()
            .then(() => updateFeedback(`Now Playing: ${playlist[currentTrack].title}`))
            .catch(error => console.error("Playback failed:", error));
    }

    // Update the feedback message on the UI
    function updateFeedback(message) {
        feedback.textContent = message;
    }

    // Update artwork saturation based on the current track
    function updateArtworkSaturation(trackIndex) {
        const images = tracksContainer.getElementsByTagName('img');
        for (let i = 0; i < images.length; i++) {
            images[i].style.filter = i === trackIndex ? 'saturate(100%)' : 'saturate(30%)';
        }
    }

    // Populate the track list in the UI
    function populateTracks() {
        playlist.forEach((track, index) => {
            const img = document.createElement('img');
            img.src = track.artwork;
            img.alt = `Artwork for ${track.title}`;
            img.addEventListener('click', () => {
                currentTrack = index;
                setTrack(index);
                playTrack();
            });

            const trackDiv = document.createElement('div');
            trackDiv.classList.add('trackItem');
            trackDiv.appendChild(img);
            tracksContainer.appendChild(trackDiv);
        });
    }

    // Update the media session for lock screen controls
    function updateMediaSession(trackIndex) {
        if ('mediaSession' in navigator) {
            const track = playlist[trackIndex];
            navigator.mediaSession.metadata = new MediaMetadata({
                title: track.title,
                artist: track.artist,
                album: track.album,
                artwork: [{ src: track.artwork, sizes: '512x512', type: 'image/png' }]
            });

            navigator.mediaSession.setActionHandler('previoustrack', () => {
                currentTrack = (currentTrack - 1 + playlist.length) % playlist.length;
                setTrack(currentTrack);
                playTrack();
            });

            navigator.mediaSession.setActionHandler('nexttrack', () => {
                currentTrack = (currentTrack + 1) % playlist.length;
                setTrack(currentTrack);
                playTrack();
            });
        }
    }

    initializeAudio();
    populateTracks();
});