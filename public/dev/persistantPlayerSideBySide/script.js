document.addEventListener('DOMContentLoaded', function () {
    const audioPlayer = document.getElementById('audioPlayer');
    const prevButton = document.getElementById('prevButton');
    const nextButton = document.getElementById('nextButton');
    const feedback = document.getElementById('feedback');
    const tracksContainer = document.getElementById('tracksContainer');

    let playlist = [
        {
            src: '../../assets/audio-001.mp3',
            title: 'First Track',
            artist: 'Artist Name',
            album: 'Album Title',
            artwork: '../../assets/audio1.drawio.png',
        },
        {
            src: '../../assets/audio-002.mp3',
            title: 'Second Track',
            artist: 'Artist Name',
            album: 'Album Title',
            artwork: '../../assets/audio2.drawio.png',
        },
        {
            src: '../../assets/audio-003.mp3',
            title: 'Third Track',
            artist: 'Artist Name',
            album: 'Album Title',
            artwork: '../../assets/audio3.drawio.png',
        },
    ];

    let currentTrack = 0;

    function updateFeedback(message) {
        if (feedback) {
            feedback.textContent = message;
        } else {
            console.warn("Feedback element not found.");
        }
    }

    function populateTracks() {
        playlist.forEach((track, index) => {
            const trackDiv = document.createElement('div');
            trackDiv.classList.add('trackItem');
            trackDiv.innerHTML = `<img src="${track.artwork}" alt="Artwork for ${track.title}">`;
            trackDiv.addEventListener('click', () => {
                currentTrack = index;
                playCurrentTrack();
            });
            tracksContainer.appendChild(trackDiv);
        });
    }

    function loadTrack(trackIndex) {
        const track = playlist[trackIndex];
        audioPlayer.src = track.src;

        document.querySelectorAll('.trackItem').forEach((item, index) => {
            if (index === trackIndex) {
                item.classList.add('playing');
            } else {
                item.classList.remove('playing');
            }
        });

        updateFeedback(`Now Playing: ${track.title} by ${track.artist}`);

        audioPlayer.play().then(() => {
            updateFeedback(`Playing: ${track.title} by ${track.artist}`);
        }).catch(error => {
            console.error("Playback failed", error);
            updateFeedback('Playback failed. Please interact with the page and try again.');
        });

        if ('mediaSession' in navigator) {
            navigator.mediaSession.metadata = new MediaMetadata({
                title: track.title,
                artist: track.artist,
                album: track.album,
                artwork: [{ src: track.artwork, sizes: '96x96', type: 'image/png' }],
            });

            navigator.mediaSession.setActionHandler('previoustrack', () => {
                playPrevious();
            });

            navigator.mediaSession.setActionHandler('nexttrack', () => {
                playNext();
            });
        }
    }

    function playCurrentTrack() {
        loadTrack(currentTrack);
    }

    function playNext() {
        currentTrack = (currentTrack + 1) % playlist.length;
        playCurrentTrack();
    }

    function playPrevious() {
        currentTrack = (currentTrack - 1 + playlist.length) % playlist.length;
        playCurrentTrack();
    }

    prevButton.addEventListener('click', playPrevious);
    nextButton.addEventListener('click', playNext);

    audioPlayer.addEventListener('ended', playCurrentTrack);

    populateTracks(); // Call to populate tracks on page load
});