document.addEventListener('DOMContentLoaded', function () {
    const artworksContainer = document.getElementById('artworksContainer');
    const audioElement = new Audio();
    let playlist = [];
    let currentTrackIndex = 0;
    const assetsPath = "../../assets/"; // Define the path to the assets folder here

    // Fetch and load the playlist from the assets folder
    fetch(`${assetsPath}playlist.json`)
        .then(response => response.json())
        .then(data => {
            playlist = data;
            initializePlayer();
        })
        .catch(error => console.error('Failed to load playlist:', error));

    function initializePlayer() {
        playlist.forEach((track, index) => {
            const img = document.createElement('img');
            img.src = `${assetsPath}${track.artwork}`; // Use assetsPath variable
            img.alt = track.title;
            img.addEventListener('click', () => {
                currentTrackIndex = index;
                playTrack(index, true); // Force play even if the same track is selected
            });

            const artworkDiv = document.createElement('div');
            artworkDiv.className = 'artwork';
            artworkDiv.appendChild(img);
            artworksContainer.appendChild(artworkDiv);
        });

        setupMediaSession();
    }

    function playTrack(index, force = false) {
        const track = playlist[index];
        if (audioElement.src !== `${assetsPath}${track.src}` || force) {
            audioElement.src = `${assetsPath}${track.src}`; // Use assetsPath variable
            audioElement.loop = true;
            audioElement.play()
                .then(() => updateMediaSession(index))
                .catch(err => console.error('Playback initiation failed:', err));
        }
    }

    function updateMediaSession(index) {
        const track = playlist[index];
        if ('mediaSession' in navigator) {
            navigator.mediaSession.metadata = new MediaMetadata({
                title: track.title,
                artist: track.artist,
                album: track.album,
                artwork: [{ src: `${assetsPath}${track.artwork}`, sizes: '512x512', type: 'image/png' }], // Use assetsPath variable
            });

            navigator.mediaSession.setActionHandler('previoustrack', () => {
                currentTrackIndex = (currentTrackIndex - 1 + playlist.length) % playlist.length;
                playTrack(currentTrackIndex, true);
            });

            navigator.mediaSession.setActionHandler('nexttrack', () => {
                currentTrackIndex = (currentTrackIndex + 1) % playlist.length;
                playTrack(currentTrackIndex, true);
            });
        }
    }

    // Ensure the Media Session actions (next/previous) trigger looped playback
    function setupMediaSession() {
        // Setup Media Session API (already handled in updateMediaSession)
    }
});