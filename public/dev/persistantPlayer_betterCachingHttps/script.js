document.addEventListener('DOMContentLoaded', function() {
    const artworksContainer = document.getElementById('artworksContainer');
    const audioElement = new Audio();
    let playlist = [];
    let currentTrackIndex = 0;
    const assetsPath = "../../assets/"; // Adjust based on your setup

    audioElement.addEventListener('ended', () => {
        // Since loop is true, this is just a fallback
        playTrack(playlist[currentTrackIndex], currentTrackIndex);
    });

    function hideLoadingScreen() {
        document.getElementById('loadingScreen').style.display = 'none';
        artworksContainer.style.display = 'flex';
    }

    function fetchPlaylist() {
        fetch(`${assetsPath}playlist.json`)
            .then(response => response.json())
            .then(data => {
                playlist = data;
                initializePlayer(playlist);
                hideLoadingScreen();
            })
            .catch(error => {
                console.error('Failed to load playlist:', error);
                hideLoadingScreen();
            });
    }

    function initializePlayer(playlist) {
        playlist.forEach((track, index) => {
            const img = document.createElement('img');
            img.src = `${assetsPath}${track.artwork}`;
            img.alt = track.title;
            img.addEventListener('click', () => {
                playTrack(track, index);
            });

            const artworkDiv = document.createElement('div');
            artworkDiv.className = 'artwork';
            artworkDiv.appendChild(img);
            artworksContainer.appendChild(artworkDiv);
        });
        setupMediaSession();
    }

    function playTrack(track, index) {
        currentTrackIndex = index;
        audioElement.src = `${assetsPath}${track.src}`;
        audioElement.loop = true;
        audioElement.play().catch(err => console.error('Playback initiation failed:', err));
        updateMediaSession(track);
    }

    function updateMediaSession(track) {
        if ('mediaSession' in navigator) {
            navigator.mediaSession.metadata = new MediaMetadata({
                title: track.title,
                artist: track.artist,
                album: track.album,
                artwork: [{ src: `${assetsPath}${track.artwork}`, sizes: '512x512', type: 'image/png' }],
            });

            navigator.mediaSession.setActionHandler('previoustrack', previousTrack);
            navigator.mediaSession.setActionHandler('nexttrack', nextTrack);
        }
    }

    function previousTrack() {
        if (currentTrackIndex === 0) {
            currentTrackIndex = playlist.length - 1; // Loop to the end
        } else {
            currentTrackIndex -= 1;
        }
        playTrack(playlist[currentTrackIndex], currentTrackIndex);
    }

    function nextTrack() {
        if (currentTrackIndex === playlist.length - 1) {
            currentTrackIndex = 0; // Loop back to the start
        } else {
            currentTrackIndex += 1;
        }
        playTrack(playlist[currentTrackIndex], currentTrackIndex);
    }

    fetchPlaylist();
});