const CACHE_NAME = 'audio-player-cache-v1';

self.addEventListener('install', event => {
    self.skipWaiting(); // Activate the service worker immediately after installation
});

self.addEventListener('fetch', event => {
    event.respondWith(
        caches.match(event.request)
            .then(response => {
                // Return the cached response if found
                if (response) {
                    return response;
                }
                // Otherwise, fetch from the network
                return fetch(event.request).then(response => {
                    // Cache the newly fetched request for future access
                    return caches.open(CACHE_NAME).then(cache => {
                        cache.put(event.request, response.clone());
                        return response;
                    });
                });
            })
    );
});