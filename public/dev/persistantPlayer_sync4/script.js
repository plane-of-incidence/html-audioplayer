document.addEventListener('DOMContentLoaded', function () {
    const tracksContainer = document.getElementById('tracksContainer');
    const feedback = document.getElementById('feedback');
    const audioElement = new Audio();
    audioElement.loop = true; // Ensure the audio loops
    

    let playlist = [
        {
            src: '../../assets/audio-001.mp3',
            title: 'First Track',
            artist: 'Artist Name',
            album: 'Album Title',
            artwork: '../../assets/audio1.drawio.png',
            length: 60480,
        },
        {
            src: '../../assets/audio-002.mp3',
            title: 'Second Track',
            artist: 'Artist Name',
            album: 'Album Title',
            artwork: '../../assets/audio2.drawio.png',
            length: 60480,
        },
        {
            src: '../../assets/audio-003.mp3',
            title: 'Third Track',
            artist: 'Artist Name',
            album: 'Album Title',
            artwork: '../../assets/audio3.drawio.png',
            length: 60480,
        },
    ];

   let currentTrackIndex = 0;

    function playTrack(trackIndex, offset) {
        const track = playlist[trackIndex];
        audioElement.src = track.src;
        audioElement.load();

        audioElement.addEventListener('loadedmetadata', () => {
            audioElement.currentTime = offset;
            audioElement.play()
                .catch(error => console.error("Playback failed:", error));
        }, { once: true });

        updateMediaSession(trackIndex);
    }
     function setTrack(trackIndex) {
        const track = playlist[trackIndex];
        audioElement.src = track.src;
        audioElement.addEventListener('canplaythrough', () => {
            audioElement.play().catch(error => console.error("Playback failed:", error));
        }, { once: true });
        updateFeedback(`Playing: ${track.title}`);
        updateMediaSession(trackIndex); // Update lock screen controls
    }

    function updateFeedback(message) {
        feedback.textContent = message;
    }

    function createArtworks() {
        playlist.forEach((track, index) => {
            const imgElem = document.createElement('img');
            imgElem.src = track.artwork;
            imgElem.alt = `Artwork for ${track.title}`;
            imgElem.addEventListener('click', () => {
                setTrack(index);
            });

            const artworkDiv = document.createElement('div');
            artworkDiv.className = 'artwork';
            artworkDiv.appendChild(imgElem);

            artworksContainer.appendChild(artworkDiv);
        });
    }

    createArtworks();

    function getCurrentTrackAndOffset() {
        const now = new Date();
        const midnight = new Date(Date.UTC(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(), 0, 0, 0));
        const msSinceMidnightUTC = now - midnight;
        const totalLength = playlist.reduce((acc, track) => acc + track.length, 0);
        const currentPosition = msSinceMidnightUTC % totalLength;
        let elapsedTime = 0;

        for (let i = 0; i < playlist.length; i++) {
            elapsedTime += playlist[i].length;
            if (elapsedTime >= currentPosition) {
                return { trackIndex: i, offset: (elapsedTime - currentPosition) / 1000 };
            }
        }
        return { trackIndex: 0, offset: 0 };
    }

    function initPlayer() {
        const { trackIndex, offset } = getCurrentTrackAndOffset();
        playTrack(trackIndex, offset);
        currentTrackIndex = trackIndex;
    }

    function updateFeedback(message) {
        feedback.textContent = message;
    }

    function updateMediaSession(trackIndex) {
        const track = playlist[trackIndex];
        if ('mediaSession' in navigator) {
            navigator.mediaSession.metadata = new MediaMetadata({
                title: track.title,
                artist: track.artist,
                album: track.album,
                artwork: [{ src: track.artwork, sizes: '512x512', type: 'image/png' }],
            });

            navigator.mediaSession.setActionHandler('previoustrack', () => {
                currentTrackIndex = (currentTrackIndex - 1 + playlist.length) % playlist.length;
                initPlayer();
            });

            navigator.mediaSession.setActionHandler('nexttrack', () => {
                currentTrackIndex = (currentTrackIndex + 1) % playlist.length;
                initPlayer();
            });

            navigator.mediaSession.setActionHandler('play', () => {
                audioElement.play();
            });

            navigator.mediaSession.setActionHandler('pause', () => {
                audioElement.pause();
            });
        }
    }

    // A user action (e.g., clicking a "Play" button) is required to initiate playback.
    document.getElementById('playButton').addEventListener('click', initPlayer);
});