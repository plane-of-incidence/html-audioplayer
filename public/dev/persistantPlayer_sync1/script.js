document.addEventListener('DOMContentLoaded', function () {
    const audioPlayer = document.getElementById('audioPlayer');
    const tracksContainer = document.getElementById('tracksContainer');
    const prevButton = document.getElementById('prevButton');
    const nextButton = document.getElementById('nextButton');
    const feedback = document.getElementById('feedback');
    audioPlayer.loop = true; // Set audio to loop

    let playlist = [
        {
            src: '../../assets/audio-001.mp3',
            title: 'First Track',
            artist: 'Artist Name',
            album: 'Album Title',
            artwork: '../../assets/audio1.drawio.png',
            length: 60480,
        },
        {
            src: '../../assets/audio-002.mp3',
            title: 'Second Track',
            artist: 'Artist Name',
            album: 'Album Title',
            artwork: '../../assets/audio2.drawio.png',
            length: 60480,
        },
        {
            src: '../../assets/audio-003.mp3',
            title: 'Third Track',
            artist: 'Artist Name',
            album: 'Album Title',
            artwork: '../../assets/audio3.drawio.png',
            length: 60480,
        },
    ];
    

      let currentTrack = 0;

    // Calculate syncStartUTC as the most recent midnight UTC
    function getMidnightUTC() {
        const now = new Date();
        const midnightUTC = new Date(Date.UTC(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(), 0, 0, 0));
        return midnightUTC.getTime();
    }

    let syncStartUTC = getMidnightUTC();

    function updateFeedback(message) {
        feedback.textContent = message;
    }

    function updateArtworkSaturation(trackIndex) {
        document.querySelectorAll('.trackItem img').forEach((img, i) => {
            img.style.filter = i === trackIndex ? 'saturate(100%)' : 'saturate(30%)';
        });
    }

    function calculateStartOffset(trackLength) {
        const nowUTC = new Date().getTime();
        const elapsedTime = nowUTC - syncStartUTC;
        const offset = elapsedTime % trackLength;
        return offset / 1000; // Convert milliseconds to seconds for audio currentTime
    }

    function loadTrack(trackIndex) {
        currentTrack = trackIndex;
        const track = playlist[trackIndex];
        audioPlayer.src = track.src;
        audioPlayer.load(); // Make sure to reload the source

        audioPlayer.addEventListener('canplay', () => {
            audioPlayer.currentTime = calculateStartOffset(track.length);
            audioPlayer.play()
                .then(() => updateFeedback(`Now Playing: ${track.title} by ${track.artist}`))
                .catch(error => console.error("Playback failed", error));
            updateArtworkSaturation(trackIndex);
        }, { once: true });

        updateMediaSession(track);
    }

    function playNext() {
        let nextTrack = (currentTrack + 1) % playlist.length;
        loadTrack(nextTrack);
    }

    function playPrevious() {
        let prevTrack = (currentTrack - 1 + playlist.length) % playlist.length;
        loadTrack(prevTrack);
    }

    function updateMediaSession(track) {
        if ('mediaSession' in navigator) {
            navigator.mediaSession.metadata = new MediaMetadata({
                title: track.title,
                artist: track.artist,
                album: track.album,
                artwork: [{ src: track.artwork, sizes: '512x512', type: 'image/png' }]
            });

            navigator.mediaSession.setActionHandler('previoustrack', playPrevious);
            navigator.mediaSession.setActionHandler('nexttrack', playNext);
        }
    }

    function populateTracks() {
        playlist.forEach((track, index) => {
            const trackDiv = document.createElement('div');
            trackDiv.classList.add('trackItem');
            const img = document.createElement('img');
            img.src = track.artwork;
            img.alt = `Artwork for ${track.title}`;
            img.addEventListener('click', () => loadTrack(index));
            trackDiv.appendChild(img);
            tracksContainer.appendChild(trackDiv);
        });
    }

    populateTracks(); // Dynamically generate the track list
});
