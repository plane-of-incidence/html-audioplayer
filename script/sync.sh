#!/bin/bash

DURATION=$1

# Duration in milliseconds for a 24-hour period
duration=$(($DURATION ))

# Get current UTC time in hours, minutes, seconds, and nanoseconds (converted to milliseconds)
utc_hours=$(date -u +%H)
utc_minutes=$(date -u +%M)
utc_seconds=$(date -u +%S)
utc_millis=$(($(date -u +%N) / 1000000))

# Convert current time to total milliseconds since midnight UTC
total_millis_since_midnight_utc=$(($utc_hours * 3600000 + $utc_minutes * 60000 + $utc_seconds * 1000 + $utc_millis))

# Calculate offset
offset=$(($total_millis_since_midnight_utc % $duration))

echo "Offset in milliseconds: $offset"
