#!/bin/bash

# Check if a file path has been provided
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <media_file>"
    exit 1
fi

# Path to the media file
MEDIA_FILE="$1"

# Check if the file exists
if [ ! -f "$MEDIA_FILE" ]; then
    echo "File does not exist: $MEDIA_FILE"
    exit 1
fi

# Use ffprobe to get the duration of the media file in seconds
DURATION_IN_SECONDS=$(ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 "$MEDIA_FILE")

# Convert the duration to milliseconds
DURATION_IN_MILLIS=$(echo "$DURATION_IN_SECONDS * 1000" | bc)

# Print the duration in milliseconds
echo "Duration: $DURATION_IN_MILLIS milliseconds"
